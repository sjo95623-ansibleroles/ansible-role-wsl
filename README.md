Role Name
=========

A brief description of the role goes here.

Requirements
------------

None

Role Variables
--------------

```
dl_url: "https://dl.mvd.ovh"
wsl_distribution: "fedora"
```

Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
        - role: ansible-role-wsl
          vars:
            dl_url: "https://dl.mvd.ovh"
            wsl_distribution: "fedora"

License
-------

Apache

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).

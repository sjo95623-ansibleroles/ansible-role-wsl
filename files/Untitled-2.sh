#!/bin/bash
pkg_mgr=
installed=
if command -v apt 2>/dev/null; then
    pkg_mgr="apt"
    installed="--installed"
elif command -v dnf 2>/dev/null; then
    pkg_mgr="dnf"
    installed="installed"
elif command -v yum 2>/dev/null; then
    pkg_mgr="yum"
    installed="installed"
else
    echo "OS not supported"
    exit 1
fi

if ! sudo ${pkg_mgr} list ${installed} ansible >/dev/null 2>&1; then
    sudo ${pkg_mgr} install ansible -y
fi

if ! command -v git 2>/dev/null; then
cat << EOF | sudo tee /tmp/bootrap.yml
- hosts: 127.0.0.1
  connection: local
  tasks: 
    - name: building git from source
      get-
    
    - package:
        name: 
EOF

fi

if [ ! -d "/opt/shell-config" ]; then
    git clone https://gitlab.com/sjo95623-ansibleplaybooks/myshell.git /opt/myshell
    sudo ansible-galaxy install -r /opt/myshell/site.yml --force
    sudo ansible-playbook /opt/myshell/site.yml
fi

